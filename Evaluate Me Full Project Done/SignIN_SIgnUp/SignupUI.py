import typing
from PyQt5 import QtCore, QtGui, QtWidgets
import hashlib

from PyQt5.QtWidgets import QWidget, QApplication, QMessageBox, QErrorMessage, QComboBox
import sys, close
import sys, back
import csv
import sys, res
import re


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(753, 581)
        Form.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        Form.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        Form.setSizeIncrement(QtCore.QSize(625, 565))
        self.widget = QtWidgets.QWidget(Form)
        self.widget.setGeometry(QtCore.QRect(70, 20, 691, 500))
        self.widget.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.widget.setObjectName("widget")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setGeometry(QtCore.QRect(40, 30, 280, 430))
        self.label.setStyleSheet(
            "border-image: url(:/Image/IMG 1.jpg);\n" "border-top-left-radius:50px;"
        )
        self.label.setText("")
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.widget)
        self.label_2.setGeometry(QtCore.QRect(310, 30, 271, 430))
        self.label_2.setStyleSheet(
            "background-color:rgba(255,255,255,255);\n"
            "border-bottom-right-radius:50px;\n"
            ""
        )
        self.label_2.setText("")
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.widget)
        self.label_3.setGeometry(QtCore.QRect(380, 80, 121, 40))
        font = QtGui.QFont()
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setStyleSheet("Color:rgba(0,0,0,200);")
        self.label_3.setObjectName("label_3")
        self.lineEdit = QtWidgets.QLineEdit(self.widget)
        self.lineEdit.setGeometry(QtCore.QRect(350, 210, 190, 40))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lineEdit.setFont(font)
        self.lineEdit.setStyleSheet(
            "background-color:rgba(0,0,0,0);\n"
            "border:none;\n"
            "border-botton:2px solid rgba(46,82,101,200);\n"
            "color:rgba(0,0,0,240);\n"
            "padding-bottom:7px;"
        )
        self.lineEdit.setObjectName("lineEdit")

        self.lineEdit_2 = QtWidgets.QLineEdit(self.widget)
        self.lineEdit_2.setGeometry(QtCore.QRect(350, 270, 190, 40))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lineEdit_2.setFont(font)
        self.lineEdit_2.setStyleSheet(
            "background-color:rgba(0,0,0,0);\n"
            "border:none;\n"
            "border-botton:2px solid rgba(46,82,101,200);\n"
            "color:rgba(0,0,0,240);\n"
            "padding-bottom:7px;"
        )
        self.lineEdit_2.setEchoMode(QtWidgets.QLineEdit.Password)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.label_4 = QtWidgets.QLabel(self.widget)
        self.label_4.setGeometry(QtCore.QRect(40, 90, 271, 311))
        self.label_4.setStyleSheet("background-color:rgba(0,0,0,75);")
        self.label_4.setText("")
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.widget)
        self.label_5.setGeometry(QtCore.QRect(40, 110, 201, 41))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label_5.setFont(font)
        self.label_5.setStyleSheet("color:rgba(255,255,255,210);")
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.widget)
        self.label_6.setGeometry(QtCore.QRect(50, 220, 241, 131))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.label_8 = QtWidgets.QLabel(self.widget)
        self.label_8.setGeometry(QtCore.QRect(90, 150, 201, 41))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label_8.setFont(font)
        self.label_8.setStyleSheet("color:rgba(255,255,255,210);")
        self.label_8.setObjectName("label_8")
        self.pushButton_2 = QtWidgets.QPushButton(self.widget)
        self.pushButton_2.setGeometry(QtCore.QRect(340, 390, 190, 40))
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75)
        self.pushButton_2.setFont(font)
        self.pushButton_2.setStyleSheet(
            "QPushButton#pushButton_2{\n"
            "   background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(11, 131, 120, 219) , stop:1 rgba(85, 98, 112, 226));\n"
            "   color:rgba(255,255,255,210);\n"
            "   bottom-radius:5px;\n"
            "}\n"
            "QPushButton#pushButton_2:hover{\n"
            "    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(150, 123, 111, 219) , stop:1 rgba(85, 81, 84, 226));\n"
            "}\n"
            "QPushButton#pushButton_2:pressed{\n"
            "   padding-left:5px;\n"
            "   padding-top:5px;\n"
            "   background-color:rgba(150,123,111,255);\n"
            "}"
        )
        self.pushButton_2.setObjectName("pushButton_2")
        self.lineEdit_3 = QtWidgets.QLineEdit(self.widget)
        self.lineEdit_3.setGeometry(QtCore.QRect(350, 150, 190, 40))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lineEdit_3.setFont(font)
        self.lineEdit_3.setStyleSheet(
            "background-color:rgba(0,0,0,0);\n"
            "border:none;\n"
            "border-botton:2px solid rgba(46,82,101,200);\n"
            "color:rgba(0,0,0,240);\n"
            "padding-bottom:7px;"
        )
        self.lineEdit_3.setObjectName("lineEdit_3")

        self.label_7 = QtWidgets.QLabel(self.widget)
        self.label_7.setGeometry(QtCore.QRect(520, 20, 71, 71))
        self.label_7.setStyleSheet(
            "background-image: url(:/close/cross.png);\n" "border:none;"
        )
        self.label_7.setText("")
        self.label_7.setPixmap(QtGui.QPixmap(":/close/cross.png"))
        self.label_7.setScaledContents(True)
        self.label_7.setObjectName("label_7")
        self.label_9 = QtWidgets.QLabel(self.widget)
        self.label_9.setGeometry(QtCore.QRect(310, 30, 51, 51))
        self.label_9.setStyleSheet("background-image: url(:/back/back.png);")
        self.label_9.setText("")
        self.label_9.setPixmap(QtGui.QPixmap(":/back/back.png"))
        self.label_9.setScaledContents(True)
        self.label_9.setObjectName("label_9")

        font = QtGui.QFont()
        font.setPointSize(10)

        # Create the QComboBox
        self.comboBox = QtWidgets.QComboBox(self.widget)
        self.comboBox.setGeometry(QtCore.QRect(350, 330, 190, 40))
        self.comboBox.addItem("Customer")
        self.comboBox.addItem("Admin")
        self.comboBox.setObjectName("comboBox")
        self.comboBox.setPlaceholderText(" Select Role ")

        # Connect signals for managing placeholder visibility

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.label_3.setText(_translate("Form", "SIGN UP"))
        self.lineEdit.setPlaceholderText(_translate("Form", "User Name"))
        self.lineEdit_2.setPlaceholderText(_translate("Form", "Password "))
        self.label_5.setText(_translate("Form", "           Welcome To "))
        self.label_6.setText(
            _translate(
                "Form",
                '"Welcome to our Super Mart, where\n'
                " shopping meets convenience.\n"
                " Discover a seamless online shopping\n"
                " experience with a wide array \n"
                ' of products at your fingertips."',
            )
        )
        self.label_8.setText(_translate("Form", "IJK Super Mart"))
        self.pushButton_2.setText(_translate("Form", "Sign Up"))
        self.lineEdit_3.setPlaceholderText(_translate("Form", "Email"))
        self.comboBox.setItemText(0, _translate("Form", "Customer"))
        self.comboBox.setItemText(1, _translate("Form", "Admin"))


import hashlib
import csv


class SignUp_App(QtWidgets.QMainWindow):
    def __init__(self):
        super(SignUp_App, self).__init__()
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.showInstructions()
        self.user_credentials = {}
        self.ui.pushButton_2.clicked.connect(self.signUp)
        self.ui.label_7.mousePressEvent = lambda event: self.close_window(event)
        self.ui.label_9.mousePressEvent = lambda event: self.showLogin(event)

    def showLogin(self, event):
        from Login import LoginApp

        self.app = QtWidgets.QApplication(sys.argv)
        self.window = LoginApp()
        self.window.show()
        SignUp_App.close(self)

    def showInstructions(self):
        QMessageBox.information(
            self,
            "instructions",
            " 1. example email: abc@gmail.com \n 2. Password min length is 7 characters and must be alphanumeric \n 3. User name can't start with digits \n 4. All fields must be required",
        )

    def close_window(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self.close()

    def signUp(self):
        email = self.ui.lineEdit_3.text()
        username = self.ui.lineEdit.text()
        password = self.ui.lineEdit_2.text()
        role = self.ui.comboBox.currentText()

        if (
            self.validate_email(email)
            and self.validate_username(username)
            and self.validate_password(password)
        ):
            if not self.already_present(username):
                hashed_password = self.hash_password(password)
                self.user_credentials[username] = {
                    "email": email,
                    "hashed_password": hashed_password,
                    "role": role,
                }
                self.write_data_to_csv()

                QMessageBox.information(self, "PopUp Message", "Signed Up Successfully")

                from Login import LoginApp

                self.app = QtWidgets.QApplication(sys.argv)

                self.window = LoginApp()
                self.window.show()
                SignUp_App.close(self)

            else:
                QMessageBox.information(
                    self, "PopUp Message", "Credentials already exists "
                )
                return

        else:
            QMessageBox.information(
                self, "PopUp Message", "Please Fill fields According to instructions"
            )
            return

    def hash_password(self, password):
        hashed = hashlib.sha256(password.encode()).hexdigest()
        return hashed

    def write_data_to_csv(self):
        fieldnames = ["username", "email", "hashed_password", "role"]
        with open(
            r"SignIN_SIgnUp\user_data.csv",
            mode="a",
            newline="",
        ) as file:
            writer = csv.DictWriter(file, fieldnames=fieldnames)

            for username, user_info in self.user_credentials.items():
                user_info["username"] = username
                writer.writerow(user_info)

    def validate_email(self, email):
        email_regex = r"^[\w\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9.-]+$"
        return re.match(email_regex, email)

    def validate_username(self, username):
        username_regex = r"^[a-zA-Z0-9_]+$"
        return re.match(username_regex, username)

    def validate_password(self, password):
        password_regex = r"^[a-zA-Z0-9@#$%^&+=]{7,}$"
        return re.match(password_regex, password)

    def already_present(self, username):
        with open(r"SignIN_SIgnUp\user_data.csv", mode="r", newline="") as file:
            reader = csv.reader(file)
            # Extract usernames from the first column (index 0)
            usernames = [row[0] for row in reader]
            if username in usernames:
                return True
        return False

    def read_user_credentials_from_csv():
        user_credentials_login = {}
        with open(r"SignIN_SIgnUp\user_data.csv", mode="r", newline="") as file:
            reader = csv.reader(file)
            for row in reader:
                if row:
                    username = row[0]
                    hashed_password = row[2]
                    role = row[3]
                    user_credentials_login[username] = [hashed_password, role]
        return user_credentials_login


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    Form = SignUp_App()

    Form.show()
    sys.exit(app.exec_())
