import pandas as pd
import matplotlib.pyplot as plt

def main():
        # Read the CSV file into a DataFrame
    df = pd.read_csv('CustomerData.csv')

    # Assuming 'ProductPrice' and 'TotalSold' columns represent the revenue
    # If not, replace 'ProductPrice' and 'TotalSold' with the actual column names representing price and quantity sold
    df['Revenue'] = df['ProductPrice'] * df['TotalSold']

    # Group by month and sum the revenue
    monthly_revenue = df.groupby('Month')['Revenue'].sum()

    # Plot the graph
    plt.figure(figsize=(10, 6))
    monthly_revenue.plot(kind='bar', color='skyblue')
    plt.title('Monthly Revenue')
    plt.xlabel('Month')
    plt.ylabel('Revenue')
    plt.xticks(rotation=45, ha='right')  # Rotate x-axis labels for better visibility
    plt.tight_layout()

    # Save the plot as an image (optional)
    plt.savefig('monthly_revenue_graph.png')

    # Display the plot
    plt.show()
