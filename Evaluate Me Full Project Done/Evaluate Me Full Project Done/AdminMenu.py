# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'AdminMenu.ui'
#
# Created by: PyQt5 UI code generator 5.15.9
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox
import typing
import sys, res
import sys, close
import sys, back
import Report
import Revenue
import sys, Image2
import sys, Img
import sys, pic
import helper
import csv
from SignIN_SIgnUp import Login
from UpdateItems import UpdateApp

import xyz


class Admin_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(910, 578)
        self.widget = QtWidgets.QWidget(Form)
        self.widget.setGeometry(QtCore.QRect(0, 0, 911, 581))
        self.widget.setStyleSheet(
            "background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:1 rgba(255, 85, 127, 255));"
        )
        self.widget.setObjectName("widget")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setGeometry(QtCore.QRect(60, 120, 801, 431))
        self.label.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label.setStyleSheet("background-image: url(:/img 3/IMG 2.jpg);")
        self.label.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label.setFrameShadow(QtWidgets.QFrame.Plain)
        self.label.setText("")
        self.label.setTextFormat(QtCore.Qt.AutoText)
        self.label.setPixmap(QtGui.QPixmap(":/img 3/IMG 2.jpg"))
        self.label.setScaledContents(True)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.tableWidget = QtWidgets.QTableWidget(self.widget)
        self.tableWidget.setGeometry(QtCore.QRect(290, 170, 541, 261))
        self.tableWidget.setStyleSheet(
            "background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:1 rgba(255, 255, 255, 255));"
        )
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.pushButton = QtWidgets.QPushButton(self.widget)
        self.pushButton.setGeometry(QtCore.QRect(120, 460, 131, 51))
        self.pushButton.setStyleSheet(
            "background-color: rgb(0, 255, 127);\n" "border: none;"
        )
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(self.widget)
        self.pushButton_2.setGeometry(QtCore.QRect(380, 460, 131, 51))
        self.pushButton_2.setStyleSheet(
            "background-color: rgb(255, 255, 0);\n" "border: none;"
        )
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_3 = QtWidgets.QPushButton(self.widget)
        self.pushButton_3.setGeometry(QtCore.QRect(620, 460, 121, 51))
        self.pushButton_3.setStyleSheet(
            "background-color: rgb(255, 0, 0);\n" "border: none;"
        )
        self.pushButton_3.setObjectName("pushButton_3")
        self.label_8 = QtWidgets.QLabel(self.widget)
        self.label_8.setGeometry(QtCore.QRect(330, 20, 271, 51))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label_8.setFont(font)
        self.label_8.setStyleSheet("color:rgba(255,255,255,210);")
        self.label_8.setAlignment(QtCore.Qt.AlignCenter)
        self.label_8.setObjectName("label_8")
        self.label_9 = QtWidgets.QLabel(self.widget)
        self.label_9.setGeometry(QtCore.QRect(270, 60, 361, 41))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label_9.setFont(font)
        self.label_9.setStyleSheet("color:rgba(255,255,255,210);")
        self.label_9.setAlignment(QtCore.Qt.AlignCenter)
        self.label_9.setObjectName("label_9")

        self.label_3 = QtWidgets.QLabel(self.widget)
        self.label_3.setGeometry(QtCore.QRect(850, 0, 61, 61))
        self.label_3.setStyleSheet("background-image: url(cross.png);")
        self.label_3.setText("")
        self.label_3.setPixmap(QtGui.QPixmap("cross.png"))
        self.label_3.setScaledContents(True)
        self.label_3.setObjectName("label_3")
        self.pushButton_4 = QtWidgets.QPushButton(self.widget)
        self.pushButton_4.setGeometry(QtCore.QRect(120, 520, 131, 51))
        self.pushButton_4.setStyleSheet(
            "background-color: rgb(255, 0, 255);\n" "border: none;"
        )
        self.pushButton_4.setObjectName("pushButton_4")
        self.pushButton_4.setText("Generate Report")

        # Generate Revenue button
        self.pushButton_5 = QtWidgets.QPushButton(self.widget)
        self.pushButton_5.setGeometry(QtCore.QRect(320, 520, 131, 51))
        self.pushButton_5.setStyleSheet(
            "background-color: rgb(0, 255, 255);\n" "border: none;"
        )
        self.pushButton_5.setObjectName("pushButton_5")
        self.pushButton_5.setText("Generate Revenue")
        self.comboBox = QtWidgets.QComboBox(self.widget)
        self.comboBox.setGeometry(QtCore.QRect(100, 260, 131, 31))
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.pushButton.setText(_translate("Form", "ADD ITEM"))
        self.pushButton_2.setText(_translate("Form", "UPDATE ITEM"))
        self.pushButton_3.setText(_translate("Form", "DELETE ITEM"))
        self.label_8.setText(_translate("Form", "IJK Super Mart"))
        self.label_9.setText(_translate("Form", "Welcome To Admin Menu"))
        self.comboBox.setItemText(0, _translate("Form", "Electronics"))
        self.comboBox.setItemText(1, _translate("Form", "Grocery"))
        self.comboBox.setItemText(2, _translate("Form", "Medicines"))
        self.comboBox.setItemText(3, _translate("Form", "Clothing"))


class FormStack:
    def __init__(self):
        self.stack = []

    def push(self, form):
        if self.stack:
            self.stack[-1].hide()  # Hide the current form
        self.stack.append(form)
        form.show()

    def pop(self):
        print(len(self.stack))
        if len(self.stack) > 1:
            self.stack.pop().close()  # Close the current form
            self.stack[-1].show()

        else:
            self.stack[0].close()


class AdminApp(QtWidgets.QMainWindow):
    def __init__(self):
        super(AdminApp, self).__init__()
        self.ui = Admin_Form()
        helper.elec_tree.populate_electronics__data_into_tree()
        helper.clothes_tree.populate_clothes_data_into_tree()
        helper.medicines_tree.populate_pharmacy__data_into_tree()
        helper.grocery_tree.populate_pharmacy__data_into_tree()
        helper.elec_tree.pre_order_traversal(helper.elec_tree.root)

        self.ui.setupUi(self)
        # Assuming your form is named "Form"
        # Inside your AdminApp class __init__ method or where you initialize the QMainWindow
        self.setWindowFlag(QtCore.Qt.WindowCloseButtonHint, False)

        self.Fill_table()
        self.ui.comboBox.currentTextChanged.connect(self.Fill_table)
        self.selected_row = None
        self.row_data = []
        self.ui.tableWidget.itemSelectionChanged.connect(self.table_row_clicked)
        self.ui.pushButton_3.clicked.connect(self.delete_product)
        self.ui.pushButton_2.clicked.connect(self.update_selected_row)
        self.ui.pushButton_4.clicked.connect(self.generate_report)
        self.ui.pushButton_5.clicked.connect(self.generate_revenue)

        ## self added
        self.ui.label_3.mousePressEvent = self.close_application
        self.ui.pushButton.clicked.connect(self.gotoAddItemsForm)

    def close_application(self, event):
        reply = QMessageBox.question(
            self,
            "Exit",
            "Are you sure you want to exit?",
            QMessageBox.Yes | QMessageBox.No,
            QMessageBox.No,
        )
        if reply == QMessageBox.Yes:
            from SignIN_SIgnUp import Login

            self.app = QtWidgets.QApplication(sys.argv)
            self.window = Login.LoginApp()
            self.window.show()
            AdminApp.close(self)

    def gotoAddItemsForm(self):
        import AddItems

        self.app = QtWidgets.QApplication(sys.argv)

        self.window = AddItems.Admin_ADD()
        self.window.show()
        AdminApp.close(self)

    def generate_report(self):
        Report.main()

    def generate_revenue(self):
        Revenue.main()

    def table_row_clicked(self):
        selected_items = self.ui.tableWidget.selectedItems()
        if selected_items:
            selected_row = selected_items[0].row()
            self.selected_row = selected_row
            self.row_data = []
            for column in range(self.ui.tableWidget.columnCount()):
                item = self.ui.tableWidget.item(selected_row, column)
                if item is not None:
                    self.row_data.append(item.text())

    def populate_table_from_csv(self, table, filename):
        with open(filename, newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            for row_index, row in enumerate(reader):
                product_id = row.get("Product_Id", "")
                product_name = row.get("Product_Name", "")
                product_price = row.get("Product_Price", "")
                product_quantity = row.get("Product_Quantity", "")

                table.insertRow(row_index)
                table.setItem(row_index, 0, QtWidgets.QTableWidgetItem(product_id))
                table.setItem(row_index, 1, QtWidgets.QTableWidgetItem(product_name))
                table.setItem(row_index, 2, QtWidgets.QTableWidgetItem(product_price))
                table.setItem(
                    row_index, 3, QtWidgets.QTableWidgetItem(product_quantity)
                )

    def delete_product(self):
        selected_row = self.ui.tableWidget.currentRow()
        if selected_row > 0:
            self.ui.tableWidget.removeRow(selected_row)
            if self.row_data[1][-1] == "E":
                helper.elec_tree.delete_product(
                    "electronics", self.row_data[0], self.row_data[1]
                )

                self.update_csv_file("electronics.csv", self.row_data[1])

            elif self.row_data[1][-1] == "G":
                helper.grocery_tree.delete_product(
                    "electronics", self.row_data[0], self.row_data[1]
                )
                self.update_csv_file("grocery.csv", self.row_data[1])

            elif self.row_data[1][-1] == "P":
                helper.medicines_tree.delete_product(
                    "pharmacy", self.row_data[0], self.row_data[1]
                )
                self.update_csv_file("medicine.csv", self.row_data[1])

            else:
                helper.clothes_tree.delete_product(
                    "clothing", self.row_data[0], self.row_data[1]
                )
                self.update_csv_file("clothes.csv", self.row_data[1])

        else:
            QMessageBox.information(self, "Row Selection", "please select a row")
            return

    def update_csv_file(self, file_name, id_to_delete):
        rows_to_keep = []
        with open(file_name, "r", newline="") as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                if row[1] != id_to_delete:
                    rows_to_keep.append(row)

        with open(file_name, "w", newline="") as csvfile:
            writer = csv.writer(csvfile)
            for row in rows_to_keep:
                writer.writerow(row)

    def Fill_table(self):
        products_table = self.ui.tableWidget
        products_table.clearContents()
        products_table.setRowCount(0)
        if self.ui.comboBox.currentText() == "Electronics":
            filename = "electronics.csv"
        elif self.ui.comboBox.currentText() == "Clothing":
            filename = "clothes.csv"

        elif self.ui.comboBox.currentText() == "Grocery":
            filename = "grocery.csv"

        else:
            filename = "medicine.csv"

        try:
            headers = [
                "Subcategory",
                "Product_Id",
                "Product_Name",
                "Product_Price",
                "Product_Quantity",
                "Product_Size",
                "Product_Color",
            ]
            products_table.setColumnCount(len(headers))
            for col, header in enumerate(headers):
                products_table.setHorizontalHeaderItem(
                    col, QtWidgets.QTableWidgetItem(header)
                )

            
            with open(filename, newline="") as csvfile:
                    reader = csv.DictReader(csvfile)

                    for row_data in reader:
                        row_position = products_table.rowCount()
                        products_table.insertRow(row_position)

                        # Importing Data to Table
                        for col, header in enumerate(headers):
                            if header in row_data:
                                data = row_data[header]
                            else:
                                if (
                                    header == "Product_Size"
                                    or header == "Product_Color"
                                ):
                                    data = "N/A"
                                else:
                                    data = "None"

                            item = QtWidgets.QTableWidgetItem(data)
                            products_table.setItem(row_position, col, item)
        except FileNotFoundError:
            print("File not found. Please check the file path.")

    def update_selected_row(self):
        if self.selected_row is not None and self.row_data is not []:
            self.app = QtWidgets.QApplication(sys.argv)

            self.window = UpdateApp(self.row_data)

            self.window.show()

            AdminApp.close(self)

        else:
            QMessageBox.information(self, "Row Selection", "please select a row")
            return


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    Form = AdminApp()

    # Push the main AdminApp form to the stack

    Form.show()
    sys.exit(app.exec_())
