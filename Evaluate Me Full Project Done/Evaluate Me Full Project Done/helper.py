import csv


class Product:
    def __init__(self, sub_category, product_id, product_name, product_price, quantity):
        self.sub_category = sub_category
        self.product_id = product_id
        self.product_name = product_name
        self.product_price = product_price
        self.quantity = quantity
        self.products_data = []


class Clothes(Product):
    def __init__(
        self,
        sub_category,
        product_id,
        product_name,
        product_price,
        size,
        color,
        quantity,
    ):
        super().__init__(
            sub_category, product_id, product_name, product_price, quantity
        )
        self.size = size
        self.color = color


class Electronics(Product):
    def __init__(self, sub_category, product_id, product_name, product_price, quantity):
        super().__init__(
            sub_category, product_id, product_name, product_price, quantity
        )


class Grocery(Product):
    def __init__(self, sub_category, product_id, product_name, product_price, quantity):
        super().__init__(
            sub_category, product_id, product_name, product_price, quantity
        )


class Medicine(Product):
    def __init__(self, sub_category, product_id, product_name, product_price, quantity):
        super().__init__(
            sub_category, product_id, product_name, product_price, quantity
        )


class TreeNode:
    def __init__(self, key):
        self.key = key
        self.children = []
        self.products = []

    def add_product(self, product):
        self.products.append(product)


class CategoryTree:
    def __init__(self, category_key):
        self.root = TreeNode(category_key)

    def add_subcategory(self, category_key, subcategory_key):
        category_node = self._search(self.root, category_key)
        if category_node:
            subcategory_node = self._search_subcategory(category_node, subcategory_key)
            if not subcategory_node:
                new_subcategory = TreeNode(subcategory_key)
                category_node.children.append(new_subcategory)
            
        else:
            print(f"Category '{category_key}' not found.")

    def delete_product(self, category_key, subcategory_key, product_id):
        category_node = self._search(self.root, category_key)
        if category_node:
            subcategory_node = self._search_subcategory(category_node, subcategory_key)
            if subcategory_node:
                products = subcategory_node.products
                for product in products:
                    if product.product_id == product_id:
                        products.remove(product)
                        print(
                            f"Product '{product.product_name}' deleted from '{subcategory_key}'."
                        )
                        return
                print(
                    f"Product with ID '{product_id}' not found in '{subcategory_key}'."
                )
            else:
                print(
                    f"Subcategory '{subcategory_key}' not found under '{category_key}'."
                )
        else:
            print(f"Category '{category_key}' not found.")

    def add_product_to_subcategory(self, category_key, subcategory_key, product):
        category_node = self._search(self.root, category_key)
        if category_node:
            subcategory_node = self._search_subcategory(category_node, subcategory_key)
            if subcategory_node:
                subcategory_node.add_product(product)

            else:
                self.add_subcategory(category_key, subcategory_key)
                subcategory_node = self._search_subcategory(
                    category_node, subcategory_key
                )
                subcategory_node.add_product(product)

        else:
            print(f"Category '{category_key}' not found.")

    def _search(self, root, key):
        if not root or root.key == key:
            return root
        for child in root.children:
            found = self._search(child, key)
            if found:
                return found
        return None

    def _search_subcategory(self, root, key):
        for child in root.children:
            if child.key == key:
                return child
        return None

    def pre_order_traversal(self, root, depth=0):
        if root:
            children_keys = [child.key for child in root.children]
            products = [product.product_name for product in root.products]
            print("  " * depth, root.key)
            if products:
                for product in products:
                    print("  " * (depth + 1), product)
            for child in root.children:
                self.pre_order_traversal(child, depth + 1)

    def print_products(self, root):
        if root:
            products = {}

            for child in root.children:
                sub_products = [product.product_name for product in child.products]
                products[child.key] = sub_products

            return products

    def print_subcategories(self, root):
        if root:
            children_keys = [child.key for child in root.children]

            print(children_keys)

    def populate_electronics__data_into_tree(self):
        with open(
            "electronics.csv",
            newline="",
        ) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                product = Electronics(
                    row["Subcategory"],
                    row["Product_Id"],
                    row["Product_Name"],
                    row["Product_Price"],
                    row["Product_Quantity"],
                )
                elec_tree.add_subcategory("electronics", row["Subcategory"])

                # Check if the product already exists before adding it
                subcategory_node = elec_tree._search_subcategory(
                    elec_tree.root, row["Subcategory"]
                )
                if subcategory_node:
                    existing_products = [
                        p.product_name for p in subcategory_node.products
                    ]
                    if row["Product_Name"] not in existing_products:
                        elec_tree.add_product_to_subcategory(
                            "electronics", row["Subcategory"], product
                        )

    def populate_clothes_data_into_tree(self):
        with open(
            "clothes.csv",
            newline="",
        ) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                product = Clothes(
                    row["Subcategory"],
                    row["Product_Id"],
                    row["Product_Name"],
                    row["Product_Price"],
                    row["Product_Size"],
                    row["Product_Color"],
                    row["Product_Quantity"],
                )
                clothes_tree.add_subcategory("clothing", row["Subcategory"])

                # Check if the product already exists before adding it
                subcategory_node = clothes_tree._search_subcategory(
                    clothes_tree.root, row["Subcategory"]
                )
                if subcategory_node:
                    existing_products = [
                        p.product_name for p in subcategory_node.products
                    ]
                    if row["Product_Name"] not in existing_products:
                        clothes_tree.add_product_to_subcategory(
                            "clothing", row["Subcategory"], product
                        )

    def populate_pharmacy__data_into_tree(self):
        with open("medicine.csv", newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                product = Medicine(
                    row["Subcategory"],
                    row["Product_Id"],
                    row["Product_Name"],
                    row["Product_Price"],
                    row["Product_Quantity"],
                )
                medicines_tree.add_subcategory("pharmacy", row["Subcategory"])

                # Check if the product already exists before adding it
                subcategory_node = medicines_tree._search_subcategory(
                    medicines_tree.root, row["Subcategory"]
                )
                if subcategory_node:
                    existing_products = [
                        p.product_name for p in subcategory_node.products
                    ]
                    if row["Product_Name"] not in existing_products:
                        medicines_tree.add_product_to_subcategory(
                            "pharmacy", row["Subcategory"], product
                        )

    def populate_grocery__data_into_tree(self):
        with open(
            "medicine.csv",
            newline="",
        ) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                product = Grocery(
                    row["Subcategory"],
                    row["Product_Id"],
                    row["Product_Name"],
                    row["Product_Price"],
                    row["Product_Quantity"],
                )
                grocery_tree.add_subcategory("groceries", row["Subcategory"])

                # Check if the product already exists before adding it
                subcategory_node = grocery_tree._search_subcategory(
                    grocery_tree.root, row["Subcategory"]
                )
                if subcategory_node:
                    existing_products = [
                        p.product_name for p in subcategory_node.products
                    ]
                    if row["Product_Name"] not in existing_products:
                        grocery_tree.add_product_to_subcategory(
                            "groceries", row["Subcategory"], product
                        )


elec_tree = CategoryTree("electronics")
clothes_tree = CategoryTree("clothing")
medicines_tree = CategoryTree("pharmacy")
grocery_tree = CategoryTree("groceries")
