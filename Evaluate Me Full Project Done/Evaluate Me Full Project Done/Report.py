import pandas as pd
import matplotlib.pyplot as plt


def main():
    file_path = "CustomerData.csv"


    df = pd.read_csv(file_path)

    # Display the first few rows of the dataframe to understand the structure of the data
    print(df.head())

    category_counts = df["ProductCategory"].value_counts()
    category_counts.plot(kind="bar", color="skyblue")
    plt.title("Product Category Distribution")
    plt.xlabel("Product Category")
    plt.ylabel("Number of Items")
    plt.savefig("product_category_distribution.png")  # Save as PNG image
    plt.show()

    sub_category_counts = df["ProductSubCategory"].value_counts()
    sub_category_counts.plot(kind="bar", color="lightcoral")
    plt.title("Product SubCategory Distribution")
    plt.xlabel("Product SubCategory")
    plt.ylabel("Number of Items")
    plt.savefig("product_subcategory_distribution.png")  # Save as PNG image
    plt.show()
