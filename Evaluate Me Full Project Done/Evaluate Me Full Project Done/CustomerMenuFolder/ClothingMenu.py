from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox
import sys
import typing
import sys,res
import sys,close
import sys,back
import sys,Image2
import sys,Img
import sys,pic
import sys, cart
from PyQt5.QtWidgets import QApplication, QMainWindow, QTableWidget, QTableWidgetItem, QPushButton, QHBoxLayout, QWidget, QSpinBox

from Helper import helper
import OverviewItems, CustomerMenu
import csv

class Ui_Form(object):
    def __init__(self, FormStack, Data, cart) -> None:
        self.MainData = Data
        self.curData = Data
        self.cart = cart
        self.curForm = FormStack[-1]
        self.FormStack = FormStack

    def setupUi(self):
        MainData = self.MainData
        temp = []
        for temp2 in MainData.values():
                temp += temp2
        self.curData = temp

        Form = self.curForm
        Form.setObjectName("Form")
        Form.resize(863, 591)
        self.widget = QtWidgets.QWidget(Form)
        self.widget.setGeometry(QtCore.QRect(0, 0, 871, 591))
        self.widget.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:1 rgba(255, 85, 127, 255));")
        self.widget.setObjectName("widget")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setGeometry(QtCore.QRect(70, 120, 751, 431))
        self.label.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label.setStyleSheet("background-image: url(:/img 3/IMG 2.jpg);")
        self.label.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label.setFrameShadow(QtWidgets.QFrame.Plain)
        self.label.setText("")
        self.label.setTextFormat(QtCore.Qt.AutoText)
        self.label.setPixmap(QtGui.QPixmap(":/img 3/IMG 2.jpg"))
        self.label.setScaledContents(True)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.label_8 = QtWidgets.QLabel(self.widget)
        self.label_8.setGeometry(QtCore.QRect(300, 0, 271, 51))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label_8.setFont(font)
        self.label_8.setStyleSheet("color:rgba(255,255,255,210);")
        self.label_8.setAlignment(QtCore.Qt.AlignCenter)
        self.label_8.setObjectName("label_8")
        self.label_9 = QtWidgets.QLabel(self.widget)
        self.label_9.setGeometry(QtCore.QRect(290, 40, 291, 41))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label_9.setFont(font)
        self.label_9.setStyleSheet("color:rgba(255,255,255,210);")
        self.label_9.setAlignment(QtCore.Qt.AlignCenter)
        self.label_9.setObjectName("label_9")
        self.label_2 = QtWidgets.QLabel(self.widget)
        self.label_2.setGeometry(QtCore.QRect(0, 0, 61, 51))
        self.label_2.setStyleSheet("background-image: url(back.png);")
        self.label_2.setText("")
        self.label_2.setPixmap(QtGui.QPixmap("back.png"))
        self.label_2.setScaledContents(True)
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.widget)
        self.label_3.setGeometry(QtCore.QRect(810, -10, 61, 61))
        self.label_3.setStyleSheet("background-image: url(cross.png);")
        self.label_3.setText("")
        self.label_3.setPixmap(QtGui.QPixmap("cross.png"))
        self.label_3.setScaledContents(True)
        self.label_3.setObjectName("label_3")
        self.label_2.mousePressEvent = self.go_back
        self.label_3.mousePressEvent = self.close_application
        self.label_10 = QtWidgets.QLabel(self.widget)
        self.label_10.setGeometry(QtCore.QRect(300, 70, 281, 41))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label_10.setFont(font)
        self.label_10.setStyleSheet("color:rgba(255,255,255,210);")
        self.label_10.setAlignment(QtCore.Qt.AlignCenter)
        self.label_10.setObjectName("label_10")
        self.label_7 = QtWidgets.QLabel(self.widget)
        self.label_7.setGeometry(QtCore.QRect(100, 370, 171, 31))
        self.label_7.setStyleSheet("font: 87 10pt \"Arial Black\";\n"
"background-color: rgb(255, 255, 0);")
        self.label_7.setAlignment(QtCore.Qt.AlignCenter)
        self.label_7.setObjectName("label_7")
        self.label_12 = QtWidgets.QLabel(self.widget)
        self.label_12.setGeometry(QtCore.QRect(100, 140, 171, 31))
        self.label_12.setStyleSheet("font: 87 10pt \"Arial Black\";\n"
"background-color: rgb(0, 255, 255);\n"
"border:none;")
        self.label_12.setAlignment(QtCore.Qt.AlignCenter)
        self.label_12.setObjectName("label_12")
        self.label_13 = QtWidgets.QLabel(self.widget)
        self.label_13.setGeometry(QtCore.QRect(100, 250, 171, 31))
        self.label_13.setStyleSheet("font: 87 10pt \"Arial Black\";\n"
"background-color: rgb(255, 85, 0);")
        self.label_13.setAlignment(QtCore.Qt.AlignCenter)
        self.label_13.setObjectName("label_13")
        self.comboBox = QtWidgets.QComboBox(self.widget)
        self.comboBox.setGeometry(QtCore.QRect(100, 190, 171, 31))
        font = QtGui.QFont()
        font.setFamily("MS Shell Dlg 2")
        font.setPointSize(8)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(9)
        self.comboBox.setFont(font)
        self.comboBox.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.comboBox.setAutoFillBackground(False)
        self.comboBox.setStyleSheet("font: 75 8pt \"MS Shell Dlg 2\";\n"
"background-color: rgb(255, 255, 255);")
        self.comboBox.setObjectName("comboBox")
        # self.comboBox.addItem("")
        # self.comboBox.addItem("")
        # self.comboBox.addItem("")
        # self.comboBox.addItem("")
        # self.comboBox.addItem("")
        # self.comboBox.addItem("")
        # self.comboBox.addItem("")
        # self.comboBox.addItem("")
        self.tableWidget = QtWidgets.QTableWidget(self.widget)
        self.tableWidget.setGeometry(QtCore.QRect(300, 150, 501, 341))
        self.tableWidget.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(5)
        self.tableWidget.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.tableWidget.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.tableWidget.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.tableWidget.setHorizontalHeaderItem(4, item)
        self.spinBox = QtWidgets.QSpinBox(self.widget)
        self.spinBox.setGeometry(QtCore.QRect(100, 420, 121, 41))
        self.spinBox.setMinimumSize(QtCore.QSize(121, 41))
        self.spinBox.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.spinBox.setObjectName("spinBox")
        self.spinBox_2 = QtWidgets.QSpinBox(self.widget)
        self.spinBox_2.setGeometry(QtCore.QRect(100, 310, 121, 41))
        self.spinBox_2.setMinimumSize(QtCore.QSize(121, 41))
        self.spinBox_2.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.spinBox_2.setObjectName("spinBox_2")
        self.pushButton = QtWidgets.QPushButton(self.widget)
        self.pushButton.setGeometry(QtCore.QRect(100, 490, 171, 31))
        self.pushButton.setStyleSheet("font: 87 12pt \"Arial Black\";\n"
"background-color: rgb(170, 0, 255);")
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(self.widget)
        self.pushButton_2.setGeometry(QtCore.QRect(620, 510, 171, 31))
        self.pushButton_2.setStyleSheet("font: 87 12pt \"Arial Black\";\n"
"background-color: rgb(0, 255, 0);")
        self.pushButton_2.setObjectName("pushButton_2")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)


        self.setUpTable()                            ###############3 addition
        self.comboBox.activated.connect(self.OnSubCategoryChaned)
        self.spinBox.valueChanged.connect(self.spin_box_changed)
        self.spinBox_2.valueChanged.connect(self.spin_box_2_changed)
        self.spinBox.setMaximum(99999)
        self.spinBox_2.setMaximum(99999) 

        self.pushButton.pressed.connect(self.updateTableWithPrice)
        self.pushButton_2.pressed.connect(self.gotoCart)

        # self.showEvent.connect(self.on_show)
        self.curForm.curUi = self

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.label_8.setText(_translate("Form", "IJK Super Mart"))
        self.label_9.setText(_translate("Form", "Welcome To Customer Menu"))
        self.label_10.setText(_translate("Form", "Clothing"))
        self.label_7.setText(_translate("Form", "Select Max Price"))
        self.label_12.setText(_translate("Form", "Select Sub-Category"))
        self.label_13.setText(_translate("Form", "Select Min Price"))
        # self.comboBox.setItemText(0, _translate("Form", "Men\'s Shirt"))
        # self.comboBox.setItemText(1, _translate("Form", "Ladies Shirt"))
        # self.comboBox.setItemText(2, _translate("Form", "Men\'s Pent"))
        # self.comboBox.setItemText(3, _translate("Form", "Ladies Pent"))
        # self.comboBox.setItemText(4, _translate("Form", "Men\'s Footwear"))
        # self.comboBox.setItemText(5, _translate("Form", "Ladies Footwear"))
        # self.comboBox.setItemText(6, _translate("Form", "Shades"))
        # self.comboBox.setItemText(7, _translate("Form", "Jackets and Dressed Suits"))
        
        self.comboBox.addItem("All")
        for idx, subCat in enumerate(self.MainData.keys()):
            self.comboBox.addItem(subCat)                            ################### addition

        
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("Form", "Product Name"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("Form", "Price"))
        item = self.tableWidget.horizontalHeaderItem(2)
        item.setText(_translate("Form", "Available Stock"))
        item = self.tableWidget.horizontalHeaderItem(3)
        item.setText(_translate("Form", "Quantity"))
        item = self.tableWidget.horizontalHeaderItem(4)
        item.setText(_translate("Form", "Add To Cart"))
        self.pushButton.setText(_translate("Form", "Apply"))
        self.pushButton_2.setText(_translate("Form", "Go To Cart"))

    def go_back(self, event):
        self.FormStack.pop()
        self.FormStack[-1].show()
        self.curForm.close()
    def close_application(self, event):
        reply = QMessageBox.question(self.curForm, 'Exit', 'Are you sure you want to exit?',
                                     QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.FormStack[0].quit()

    def gotoCart(self):
        nextForm = QtWidgets.QWidget()
        self.FormStack.append(nextForm)
        newUi = OverviewItems.Ui_Form(self.FormStack, self.cart)
        newUi.setupUi()
        nextForm.show()

        self.curForm.hide()


    def setUpTable(self):
        Data = self.curData
        table = self.tableWidget
        table.clearContents()
        table.setRowCount(len(Data))
        # table.setColumnCount(5)
        # table.setHorizontalHeaderLabels(['Name', 'Price', 'Available Stock', 'Quantity', 'Add to Cart'])
        table.verticalHeader().hide()

        # create an cell widget
        for index, obj  in enumerate(Data):
                
                whl = QSpinBox()
                table.setCellWidget(index, 3, whl)
                whl.setMaximum(int(obj.quantity))
                whl.setMinimum(min(1, int(obj.quantity)))

                btn = QPushButton("Add to cart")
                # btn.setText('add')
                btn.clicked.connect(lambda checked, r=index, obj = obj, spinWheel = whl: self.addToCartButtonClick(r, obj, spinWheel))
                table.setCellWidget(index, 4, btn)
                table.setItem(index, 0, QTableWidgetItem(obj.product_name))
                table.setItem(index, 1, QTableWidgetItem(obj.product_price))
                table.setItem(index, 2, QTableWidgetItem(obj.quantity))

    def addToCartButtonClick(self, index, obj, spinWheel):
        tbl = self.tableWidget
        if int(spinWheel.value()) == 0:
            reply=QMessageBox.warning(self.curForm, 'Sorry!', 'No Stock Available',
                                QMessageBox.Ok )
            return
        obj.quantity = str(int(obj.quantity) - int(spinWheel.value()))

        self.cart[obj] = self.cart.get(obj, 0) + int(spinWheel.value())
        # print(self.cart[obj])
        self.setUpTable()
        return

    def OnSubCategoryChaned(self):
        self.spinBox.setValue(0)
        self.spinBox_2.setValue(0)
        # Get the selected item text and update the label
        selected_item = self.comboBox.currentText()
        # self.label.setText(f"Selected Item: {selected_item}")
        if selected_item.lower() == "all":
            temp = []
            for temp2 in self.MainData.values():
                    temp += temp2
            self.curData = temp
        else:
            temp = self.MainData[selected_item]
            self.curData = temp
        self.setUpTable()
        return
             
    def spin_box_changed(self):
        if self.spinBox.value() > self.spinBox_2.value():
            self.spinBox.setValue(0)
    def spin_box_2_changed(self):
        if self.spinBox.value() > self.spinBox_2.value():
            self.spinBox_2.setValue(self.spinBox.value())

    def updateTableWithPrice(self):
        temp1, temp2 = self.spinBox.value(), self.spinBox_2.value()
        self.OnSubCategoryChaned()  ## refershing data with selected category
        temp = []
        for item in self.curData:
            price = int(item.product_price)
            print(price)
            if price >= temp1 and price <= temp2:
                temp.append(item)
        self.curData = temp
        self.setUpTable()
        
    def saveCartToCsv(self):
        fieldnames = ['Product','Subcategory', 'Product_Id', 'Product_Name', 'Product_Price','Product_Size', 'Product_Color', 'Product_Quantity']
        temp = helper.path + "\\clothes.csv"
        with open(temp, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)

            # Write header
            writer.writerow(fieldnames)

            # Write data
            for subcat, items in self.MainData.items():
                for obj in items:
                   writer.writerow([obj.product_name, subcat, obj.product_id, obj.product_name, obj.product_price, obj.size, obj.color, obj.quantity])
          
     


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

