from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, QTableWidget, QTableWidgetItem
from PyQt5.QtWidgets import QMessageBox
import sys
import typing
import sys,res
import sys,close
import sys,back
import sys,Image2
import sys,Img
import sys,pic
import sys,Electronics
import sys,clothing
import sys,grocery
import sys,pharmacy

import ClothingMenu, ElectronicsMenu, PharmacyMenu, GroceriesMenu
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout
from Helper import helper

import csv, heapq

import os
current_path = os.getcwd()


class Ui_Form(object):
    def __init__(self, FormStack, Data, heap) -> None:
        self.curForm = FormStack[-1]
        self.FormStack = FormStack
        self.mainData = Data
        self.cart = {}
        self.heap = heap
   
    def setupUi(self):
        Form = self.curForm
        Form.setObjectName("Form")
        Form.resize(1033, 579)
        print(Form.width())
        print(Form.height())
        self.widget = QtWidgets.QWidget(Form)
        self.widget.setGeometry(QtCore.QRect(0, 0, 1041, 581))
        self.widget.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:1 rgba(255, 85, 127, 255));")
        self.widget.setObjectName("widget")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setGeometry(QtCore.QRect(70, 120, 681, 431))
        self.label.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label.setStyleSheet("background-image: url(:/img 3/IMG 2.jpg);")
        self.label.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label.setFrameShadow(QtWidgets.QFrame.Plain)
        self.label.setText("")
        self.label.setTextFormat(QtCore.Qt.AutoText)
        self.label.setPixmap(QtGui.QPixmap(":/img 3/IMG 2.jpg"))
        self.label.setScaledContents(True)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.label_8 = QtWidgets.QLabel(self.widget)
        self.label_8.setGeometry(QtCore.QRect(400, 0, 271, 51))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label_8.setFont(font)
        self.label_8.setStyleSheet("color:rgba(255,255,255,210);")
        self.label_8.setAlignment(QtCore.Qt.AlignCenter)
        self.label_8.setObjectName("label_8")
        self.label_9 = QtWidgets.QLabel(self.widget)
        self.label_9.setGeometry(QtCore.QRect(400, 40, 291, 41))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label_9.setFont(font)
        self.label_9.setStyleSheet("color:rgba(255,255,255,210);")
        self.label_9.setAlignment(QtCore.Qt.AlignCenter)
        self.label_9.setObjectName("label_9")
        self.label_2 = QtWidgets.QLabel(self.widget)
        self.label_2.setGeometry(QtCore.QRect(0, 0, 61, 51))
        self.label_2.setStyleSheet(f"background-image: url(back.png);")
        self.label_2.setText("")
        self.label_2.setPixmap(QtGui.QPixmap("back.png"))
        self.label_2.setScaledContents(True)
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.widget)
        self.label_3.setGeometry(QtCore.QRect(970, -10, 61, 61))
        self.label_3.setStyleSheet("background-image: url(cross.png);")
        self.label_3.setText("")
        self.label_3.setPixmap(QtGui.QPixmap("cross.png"))
        self.label_3.setScaledContents(True)
        self.label_3.setObjectName("label_3")
        self.label_2.mousePressEvent = self.go_back
        self.label_3.mousePressEvent = self.close_application
        self.label_10 = QtWidgets.QLabel(self.widget)
        self.label_10.setGeometry(QtCore.QRect(380, 70, 281, 41))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label_10.setFont(font)
        self.label_10.setStyleSheet("color:rgba(255,255,255,210);")
        self.label_10.setAlignment(QtCore.Qt.AlignCenter)
        self.label_10.setObjectName("label_10")
        self.label_6 = QtWidgets.QLabel(self.widget)
        self.label_6.setGeometry(QtCore.QRect(490, 490, 131, 31))
        self.label_6.setStyleSheet("font: 87 10pt \"Arial Black\";\n"
"background-color: rgb(85, 0, 255);")
        self.label_6.setAlignment(QtCore.Qt.AlignCenter)
        self.label_6.setObjectName("label_6")
        self.label_7 = QtWidgets.QLabel(self.widget)
        self.label_7.setGeometry(QtCore.QRect(170, 490, 131, 31))
        self.label_7.setStyleSheet("font: 87 10pt \"Arial Black\";\n"
"background-color: rgb(255, 255, 0);")
        self.label_7.setAlignment(QtCore.Qt.AlignCenter)
        self.label_7.setObjectName("label_7")
        self.label_12 = QtWidgets.QLabel(self.widget)
        self.label_12.setGeometry(QtCore.QRect(170, 280, 131, 31))
        self.label_12.setStyleSheet("font: 87 10pt \"Arial Black\";\n"
"background-color: rgb(0, 255, 255);\n"
"border:none;")
        self.label_12.setAlignment(QtCore.Qt.AlignCenter)
        self.label_12.setObjectName("label_12")
        self.label_13 = QtWidgets.QLabel(self.widget)
        self.label_13.setGeometry(QtCore.QRect(490, 280, 131, 31))
        self.label_13.setStyleSheet("font: 87 10pt \"Arial Black\";\n"
"background-color: rgb(255, 85, 0);")
        self.label_13.setAlignment(QtCore.Qt.AlignCenter)
        self.label_13.setObjectName("label_13")
        self.label_4 = QtWidgets.QLabel(self.widget)
        self.label_4.setGeometry(QtCore.QRect(130, 140, 221, 121))
        self.label_4.setStyleSheet("background-image: url(:/ElectronicsImg/Electronics.jpg);")
        self.label_4.setText("")
        self.label_4.setPixmap(QtGui.QPixmap(":/ElectronicsImg/Electronics.jpg"))
        self.label_4.setScaledContents(True)
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.widget)
        self.label_5.setGeometry(QtCore.QRect(450, 140, 211, 121))
        self.label_5.setStyleSheet("background-image: url(:/clothingimg/clothing.jpg);")
        self.label_5.setText("")
        self.label_5.setPixmap(QtGui.QPixmap(":/clothingimg/clothing.jpg"))
        self.label_5.setScaledContents(True)
        self.label_5.setObjectName("label_5")
        self.label_11 = QtWidgets.QLabel(self.widget)
        self.label_11.setGeometry(QtCore.QRect(130, 350, 211, 121))
        self.label_11.setStyleSheet("background-image: url(:/groceryimg/grocery.jpg);")
        self.label_11.setText("")
        self.label_11.setPixmap(QtGui.QPixmap(":/groceryimg/grocery.jpg"))
        self.label_11.setScaledContents(True)
        self.label_11.setObjectName("label_11")
        self.label_14 = QtWidgets.QLabel(self.widget)
        self.label_14.setGeometry(QtCore.QRect(440, 350, 231, 121))
        self.label_14.setText("")
        self.label_14.setPixmap(QtGui.QPixmap(":/pharmacyimg/pharmacy.jpg"))
        self.label_14.setScaledContents(True)
        self.label_14.setObjectName("label_14")
        self.tableWidget = QtWidgets.QTableWidget(self.widget)
        self.tableWidget.setGeometry(QtCore.QRect(770, 210, 231, 291))
        self.tableWidget.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.label_15 = QtWidgets.QLabel(self.widget)
        self.label_15.setGeometry(QtCore.QRect(770, 140, 231, 51))
        self.label_15.setStyleSheet("background-color: rgb(0, 170, 255);\n"
"font: 87 12pt \"Arial Black\";")
        self.label_15.setAlignment(QtCore.Qt.AlignCenter)
        self.label_15.setObjectName("label_15")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

        self.label_4.mousePressEvent = self.gotoElectronicsUI
        self.label_12.mousePressEvent = self.gotoElectronicsUI

        self.label_5.mousePressEvent = self.gotoClothingUI
        self.label_13.mousePressEvent = self.gotoClothingUI

        self.label_7.mousePressEvent = self.gotoGroceriesUI
        self.label_11.mousePressEvent = self.gotoGroceriesUI

        self.label_6.mousePressEvent = self.gotoPharmacyUI
        self.label_14.mousePressEvent = self.gotoPharmacyUI



    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.label_8.setText(_translate("Form", "IJK Super Mart"))
        self.label_9.setText(_translate("Form", "Welcome To Customer Menu"))
        self.label_10.setText(_translate("Form", "Select Category"))
        self.label_6.setText(_translate("Form", "Pharmacy"))
        self.label_7.setText(_translate("Form", "Grocery"))
        self.label_12.setText(_translate("Form", "Electronics"))
        self.label_13.setText(_translate("Form", "Clothing"))
        self.label_15.setText(_translate("Form", "Trending Items"))

        self.tempFillBox()
        self.curForm.curUi = self


    def tempFillBox(self):
        self.removeDuplicatesFromHeap(self.heap)
        heapq.heapify(self.heap)
        count = min(10, len(self.heap))
        temp = []
        table = self.tableWidget
        table.clearContents()
        table.setRowCount(count)
        table.setColumnCount(1)
        for i in range(count):
                
                temp.append(heapq.heappop(self.heap))
                table.setItem(i, 0, QTableWidgetItem(temp[-1][1].product_name))
                # btn = QPushButton(str(i))
                # btn.clicked.connect(lambda checked, index = i: self.gotoCategoryUI(index))
                # table.setCellWidget(i, 0, btn)
        for i in temp:
            heapq.heappush(self.heap, i)
    
    def gotoElectronicsUI(self, event):
        nextForm = QtWidgets.QWidget()
        self.FormStack.append(nextForm)
        newUi = None
        newUi = ElectronicsMenu.Ui_Form(self.FormStack, self.mainData[0], self.cart)
        newUi.setupUi()
        nextForm.show()

        self.curForm.hide()   
        
        return

    def gotoClothingUI(self, event):
        nextForm = QtWidgets.QWidget()
        self.FormStack.append(nextForm)
        newUi = None
        newUi = ClothingMenu.Ui_Form(self.FormStack, self.mainData[1], self.cart)
        newUi.setupUi()
        nextForm.show()

        self.curForm.hide()   
        
        return

    def gotoPharmacyUI(self, event):
        nextForm = QtWidgets.QWidget()
        self.FormStack.append(nextForm)
        newUi = None
        newUi = PharmacyMenu.Ui_Form(self.FormStack, self.mainData[2], self.cart)
        newUi.setupUi()
        nextForm.show()

        self.curForm.hide()   
        
        return

    def gotoGroceriesUI(self, event):
        nextForm = QtWidgets.QWidget()
        self.FormStack.append(nextForm)
        newUi = None
        newUi = GroceriesMenu.Ui_Form(self.FormStack, self.mainData[3], self.cart)
        newUi.setupUi()
        nextForm.show()

        self.curForm.hide()   
        
        return
    

    def go_back(self, event):
        self.FormStack.pop()
        self.FormStack[-1].show()
        self.curForm.close()

    def close_application(self, event):
        reply = QMessageBox.question(self.curForm, 'Exit', 'Are you sure you want to exit?',
                                     QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.FormStack[0].quit()
    

    def saveAllDataProducts(self):

        fieldnames = ['Subcategory', 'Product_Id', 'Product_Name', 'Product_Price', 'Product_Quantity']
        temp = "electronics.csv"
        with open(temp, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)

            # Write header
            writer.writerow(fieldnames)

            # Write data
            for subcat, items in self.mainData[0].items():
                for obj in items:
                    writer.writerow([ subcat, obj.product_id, obj.product_name, obj.product_price,obj.quantity])
        fieldnames = ['Subcategory', 'Product_Id', 'Product_Name', 'Product_Price', 'Product_Quantity']
        temp = "medicine.csv"
        with open(temp, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)

            # Write header
            writer.writerow(fieldnames)

            # Write data
            for subcat, items in self.mainData[2].items():
                for obj in items:
                    writer.writerow([ subcat, obj.product_id, obj.product_name, obj.product_price,obj.quantity])

        fieldnames = ['Subcategory', 'Product_Id', 'Product_Name', 'Product_Price', 'Product_Quantity']
        temp = "grocery.csv"
        with open(temp, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)

            # Write header
            writer.writerow(fieldnames)

            # Write data
            for subcat, items in self.mainData[3].items():
                for obj in items:
                    writer.writerow([ subcat, obj.product_id, obj.product_name, obj.product_price,obj.quantity])

        fieldnames = ['Product','Subcategory', 'Product_Id', 'Product_Name', 'Product_Price','Product_Size', 'Product_Color', 'Product_Quantity']
        temp = "clothes.csv"
        with open(temp, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)

            # Write header
            writer.writerow(fieldnames)

            # Write data
            for subcat, items in self.mainData[1].items():
                for obj in items:
                    writer.writerow([obj.product_name, subcat, obj.product_id, obj.product_name, obj.product_price, obj.size, obj.color, obj.quantity])

    def removeDuplicatesFromHeap(self, heap):
        set1 = set()
        for i in range(len(heap)):
            while heap[i][0] in set1:
                heap[i][0] -= 0.01
            set1.add(heap[i][0])


# if __name__ == "__main__":
#     app = QtWidgets.QApplication(sys.argv)
#     Form = QtWidgets.QWidget()
#     ui = Ui_Form()
#     ui.setupUi(Form)
#     Form.show()
#     sys.exit(app.exec_())
