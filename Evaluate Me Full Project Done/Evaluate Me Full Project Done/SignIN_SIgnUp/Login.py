from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox, QErrorMessage

import sys, res

path1 = r"E:\DSA\Final Project New\Evaluate Me Full Project Done\Evaluate Me Full Project Done"  # Starting Path
sys.path.append(path1)
sys.path.append(path1 + "\\CustomerMenuFolder")

import sys, close


import hashlib

from CustomerMenuFolder.Helper import helper
from CustomerMenuFolder import CustomerMenu


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(753, 581)
        Form.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        Form.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        Form.setSizeIncrement(QtCore.QSize(625, 565))
        self.widget = QtWidgets.QWidget(Form)
        self.widget.setGeometry(QtCore.QRect(30, 30, 691, 500))
        self.widget.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.widget.setObjectName("widget")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setGeometry(QtCore.QRect(40, 30, 280, 430))
        self.label.setStyleSheet(
            "border-image: url(:/Image/IMG 1.jpg);\n" "border-top-left-radius:50px;"
        )
        self.label.setText("")
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.widget)
        self.label_2.setGeometry(QtCore.QRect(310, 30, 271, 430))
        self.label_2.setStyleSheet(
            "background-color:rgba(255,255,255,255);\n"
            "border-bottom-right-radius:50px;\n"
            ""
        )
        self.label_2.setText("")
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.widget)
        self.label_3.setGeometry(QtCore.QRect(400, 80, 100, 40))
        font = QtGui.QFont()
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setStyleSheet("Color:rgba(0,0,0,200);")
        self.label_3.setObjectName("label_3")
        self.lineEdit = QtWidgets.QLineEdit(self.widget)
        self.lineEdit.setGeometry(QtCore.QRect(350, 150, 190, 40))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lineEdit.setFont(font)
        self.lineEdit.setStyleSheet(
            "background-color:rgba(0,0,0,0);\n"
            "border:none;\n"
            "border-botton:2px solid rgba(46,82,101,200);\n"
            "color:rgba(0,0,0,240);\n"
            "padding-bottom:7px;"
        )
        self.lineEdit.setObjectName("lineEdit")
        self.lineEdit_2 = QtWidgets.QLineEdit(self.widget)
        self.lineEdit_2.setGeometry(QtCore.QRect(350, 210, 190, 40))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lineEdit_2.setFont(font)
        self.lineEdit_2.setStyleSheet(
            "background-color:rgba(0,0,0,0);\n"
            "border:none;\n"
            "border-botton:2px solid rgba(46,82,101,200);\n"
            "color:rgba(0,0,0,240);\n"
            "padding-bottom:7px;"
        )
        self.lineEdit_2.setEchoMode(QtWidgets.QLineEdit.Password)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.pushButton = QtWidgets.QPushButton(self.widget)
        self.pushButton.setGeometry(QtCore.QRect(350, 300, 190, 40))
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75)
        self.pushButton.setFont(font)
        self.pushButton.setStyleSheet(
            "QPushButton#pushButton{\n"
            "   background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(11, 131, 120, 219) , stop:1 rgba(85, 98, 112, 226));\n"
            "   color:rgba(255,255,255,210);\n"
            "   bottom-radius:5px;\n"
            "}\n"
            "QPushButton#pushButton:hover{\n"
            "    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(150, 123, 111, 219) , stop:1 rgba(85, 81, 84, 226));\n"
            "}\n"
            "QPushButton#pushButton:pressed{\n"
            "   padding-left:5px;\n"
            "   padding-top:5px;\n"
            "   background-color:rgba(150,123,111,255);\n"
            "}"
        )
        self.pushButton.setObjectName("pushButton")
        self.label_4 = QtWidgets.QLabel(self.widget)
        self.label_4.setGeometry(QtCore.QRect(40, 90, 271, 311))
        self.label_4.setStyleSheet("background-color:rgba(0,0,0,75);")
        self.label_4.setText("")
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.widget)
        self.label_5.setGeometry(QtCore.QRect(40, 110, 201, 41))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label_5.setFont(font)
        self.label_5.setStyleSheet("color:rgba(255,255,255,210);")
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.widget)
        self.label_6.setGeometry(QtCore.QRect(50, 220, 241, 131))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.label_8 = QtWidgets.QLabel(self.widget)
        self.label_8.setGeometry(QtCore.QRect(90, 150, 201, 41))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label_8.setFont(font)
        self.label_8.setStyleSheet("color:rgba(255,255,255,210);")
        self.label_8.setObjectName("label_8")
        self.pushButton_2 = QtWidgets.QPushButton(self.widget)
        self.pushButton_2.setGeometry(QtCore.QRect(350, 360, 190, 40))
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75)
        self.pushButton_2.setFont(font)
        self.pushButton_2.setStyleSheet(
            "QPushButton#pushButton_2{\n"
            "   background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(11, 131, 120, 219) , stop:1 rgba(85, 98, 112, 226));\n"
            "   color:rgba(255,255,255,210);\n"
            "   bottom-radius:5px;\n"
            "}\n"
            "QPushButton#pushButton_2:hover{\n"
            "    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(150, 123, 111, 219) , stop:1 rgba(85, 81, 84, 226));\n"
            "}\n"
            "QPushButton#pushButton_2:pressed{\n"
            "   padding-left:5px;\n"
            "   padding-top:5px;\n"
            "   background-color:rgba(150,123,111,255);\n"
            "}"
        )
        self.pushButton_2.setObjectName("pushButton_2")
        self.label_7 = QtWidgets.QLabel(self.widget)
        self.label_7.setGeometry(QtCore.QRect(380, 410, 131, 31))
        self.label_7.setAlignment(QtCore.Qt.AlignCenter)
        self.label_7.setObjectName("label_7")
        self.label_9 = QtWidgets.QLabel(self.widget)
        self.label_9.setGeometry(QtCore.QRect(520, 20, 71, 71))
        self.label_9.setStyleSheet(
            "background-image: url(:/close/cross.png);\n" "border:none;"
        )
        self.label_9.setText("")
        self.label_9.setPixmap(QtGui.QPixmap(":/close/cross.png"))
        self.label_9.setScaledContents(True)
        self.label_9.setObjectName("label_9")

        self.checkBox = QtWidgets.QCheckBox(self.widget)
        self.checkBox.setGeometry(QtCore.QRect(350, 270, 70, 17))
        self.checkBox.setObjectName("checkBox")
        self.checkBox_2 = QtWidgets.QCheckBox(self.widget)
        self.checkBox_2.setGeometry(QtCore.QRect(460, 270, 70, 17))
        self.checkBox_2.setObjectName("checkBox_2")
        self.checkBox.stateChanged.connect(self.on_checkBox_stateChanged)
        self.checkBox_2.stateChanged.connect(self.on_checkBox_2_stateChanged)
        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def on_checkBox_stateChanged(self, state):
        if state == QtCore.Qt.Checked:
            self.checkBox_2.setChecked(False)

    def on_checkBox_2_stateChanged(self, state):
        if state == QtCore.Qt.Checked:
            self.checkBox.setChecked(False)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.label_3.setText(_translate("Form", "LOG IN"))
        self.lineEdit.setPlaceholderText(_translate("Form", "User Name"))
        self.lineEdit_2.setPlaceholderText(_translate("Form", "Password"))
        self.pushButton.setText(_translate("Form", "LOG IN"))
        self.label_5.setText(_translate("Form", "           Welcome To "))
        self.label_6.setText(
            _translate(
                "Form",
                '"Welcome to our Super Mart, where\n'
                " shopping meets convenience.\n"
                " Discover a seamless online shopping\n"
                " experience with a wide array \n"
                ' of products at your fingertips."',
            )
        )
        self.label_8.setText(_translate("Form", "IJK Super Mart"))
        self.pushButton_2.setText(_translate("Form", "Sign Up"))
        self.label_7.setText(_translate("Form", "Create New Account"))
        self.checkBox.setText(_translate("Form", "Admin"))
        self.checkBox_2.setText(_translate("Form", "Customer"))


class LoginApp(QtWidgets.QMainWindow):
    def __init__(self):
        super(LoginApp, self).__init__()
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.ui.pushButton.clicked.connect(self.login)
        self.ui.pushButton_2.clicked.connect(self.signUp_show)
        self.ui.label_9.mousePressEvent = lambda event: self.close_window(event)

    def close_window(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self.close()

    def login(self):
        username = self.ui.lineEdit.text()
        password = self.ui.lineEdit_2.text()
        role = ""
        if self.ui.checkBox.isChecked():
            role = "Admin"

        elif self.ui.checkBox_2.isChecked():
            role = "Customer"

        value = self.credentails_check(username, password, role)

    def credentails_check(self, username, password, role):
        from SignupUI import SignUp_App

        user_credentials = SignUp_App.read_user_credentials_from_csv()
        if username in user_credentials:
            stored_hashed_password = user_credentials[username][0]
            stored_role = user_credentials[username][1]
            hashed_password = self.hash_password(password)
            if stored_hashed_password == hashed_password and stored_role == role:
                if role == "Admin":
                    from AdminMenu import AdminApp

                    QMessageBox.information(self, "", f" Welcome {username} ")
                    self.app = QtWidgets.QApplication(sys.argv)

                    self.window = AdminApp()
                    self.window.show()
                    LoginApp.close(self)

                else:  # Connect Customer form here
                    while len(FormStack) > 1:
                        FormStack.pop()
                    eData = helper.loadElectronicData()
                    gData = helper.loadGroceryData()
                    mData = helper.loadMedicineData()
                    cData = helper.loadClothesData()
                    ## get gdata, pdata, cdata and put them in list
                    Data = [eData, cData, mData, gData]
                    heap = helper.readFromCustomerData(Data)

                    # app = QtWidgets.QApplication(sys.argv)
                    Form = QtWidgets.QWidget()
                    FormStack.append(self)
                    FormStack.append(Form)
                    ui = CustomerMenu.Ui_Form(FormStack, Data=Data, heap=heap)
                    ui.setupUi()
                    self.hide()
                    self.ui.lineEdit.setText("")
                    self.ui.lineEdit_2.setText("")
                    Form.show()

            else:
                QMessageBox.information(self, "", "INVALID Password ")
                return False

        else:
            QMessageBox.information(self, "", "INVALID CREDENTIALS")
            return False

    def hash_password(self, password):
        hashed = hashlib.sha256(password.encode()).hexdigest()
        return hashed

    def signUp_show(self):
        self.app = QtWidgets.QApplication(sys.argv)
        from SignupUI import SignUp_App

        self.window = SignUp_App()
        self.window.show()
        LoginApp.close(self)


FormStack = []
if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    Form = LoginApp()
    Form.show()
    FormStack = [app]
    sys.exit(app.exec_())
