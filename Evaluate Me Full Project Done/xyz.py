from PyQt5 import QtCore, QtGui, QtWidgets
import typing
import sys, res
import sys, close
import sys, back
import sys, Image2
import sys, Img
import sys, pic
import helper
import csv

import AdminMenu


class FormStack:
    def __init__(self):
        self.stack = []

    def push(self, form):
        if self.stack:
            self.stack[-1].hide()  # Hide the current form
        self.stack.append(form)
        form.show()

    def pop(self):
        if len(self.stack) > 1:
            self.stack.pop().close()  # Close the current form
            self.stack[-1].show()
