import csv
from datetime import datetime
import heapq

# path1 = "D:\\UET BSCS\\DSA\\Final Project\\helper_ijlal"

class Product:
    product_counter = 0

    def __init__(self, sub_category, product_id, product_name, product_price, quantity):
        self.sub_category = sub_category
        self.product_id = product_id
        self.product_name = product_name
        self.product_price = product_price
        self.quantity = quantity
        self.products_data = []


class Clothes(Product):
    def __init__(
        self,
        sub_category,
        product_id,
        product_name,
        product_price,
        size,
        color,
        quantity,
    ):
        super().__init__(
            sub_category, product_id, product_name, product_price, quantity
        )
        self.size = size
        self.color = color
    def giveCategory(self):
        return "Clothes"


class Electronics(Product):
    def __init__(self, sub_category, product_id, product_name, product_price, quantity):
        super().__init__(
            sub_category, product_id, product_name, product_price, quantity
        )
    def giveCategory(self):
        return "Electronics"


class Grocery(Product):
    def __init__(self, sub_category, product_id, product_name, product_price, quantity):
        super().__init__(
            sub_category, product_id, product_name, product_price, quantity
        )
    def giveCategory(self):
        return "Grocery"


class Medicine(Product):
    def __init__(self, sub_category, product_id, product_name, product_price, quantity):
        super().__init__(
            sub_category, product_id, product_name, product_price, quantity
        )
    def giveCategory(self):
        return "Medicine"


class TreeNode:
    def __init__(self, key):
        self.key = key
        self.children = []
        self.products = []

    def add_product(self, product):
        self.products.append(product)


class CategoryTree:
    def __init__(self, category_key):
        self.root = TreeNode(category_key)

    def add_subcategory(self, category_key, subcategory_key):
        category_node = self._search(self.root, category_key)
        if category_node:
            new_subcategory = TreeNode(subcategory_key)
            category_node.children.append(new_subcategory)
        else:
            print(f"Category '{category_key}' not found.")

    # def add_product_to_subcategory(self, category_key, subcategory_key, product):
    #     category_node = self._search(self.root, category_key)
    #     if category_node:
    #         subcategory_node = self._search_subcategory(category_node, subcategory_key)
    #         if subcategory_node:
    #             subcategory_node.add_product(product)
    #         else:
    #             print(
    #                 f"Subcategory '{subcategory_key}' not found under '{category_key}'."
    #             )
    #     else:
    #         print(f"Category '{category_key}' not found.")

    def add_clothes_to_subcategory(self, category_key, subcategory_key, product):
        category_node = self._search(self.root, category_key)
        if category_node:
            subcategory_node = self._search_subcategory(category_node, subcategory_key)
            if subcategory_node:
                subcategory_node.add_product(product)
            else:
                print(
                    f"Subcategory '{subcategory_key}' not found under '{category_key}'."
                )
        else:
            print(f"Category '{category_key}' not found.")

    def add_electronics_to_subcategory(self, category_key, subcategory_key, product):
        category_node = self._search(self.root, category_key)
        if category_node:
            subcategory_node = self._search_subcategory(category_node, subcategory_key)
            if subcategory_node:
                subcategory_node.add_product(product)
            else:
                print(
                    f"Subcategory '{subcategory_key}' not found under '{category_key}'."
                )
        else:
            print(f"Category '{category_key}' not found.")

    def add_grocery_to_subcategory(self, category_key, subcategory_key, product):
        category_node = self._search(self.root, category_key)
        if category_node:
            subcategory_node = self._search_subcategory(category_node, subcategory_key)
            if subcategory_node:
                subcategory_node.add_product(product)
            else:
                print(
                    f"Subcategory '{subcategory_key}' not found under '{category_key}'."
                )
        else:
            print(f"Category '{category_key}' not found.")

    def add_medicines_to_subcategory(self, category_key, subcategory_key, product):
        category_node = self._search(self.root, category_key)
        if category_node:
            subcategory_node = self._search_subcategory(category_node, subcategory_key)
            if subcategory_node:
                subcategory_node.add_product(product)
            else:
                print(
                    f"Subcategory '{subcategory_key}' not found under '{category_key}'."
                )
        else:
            print(f"Category '{category_key}' not found.")

    def _search(self, root, key):
        if not root or root.key == key:
            return root
        for child in root.children:
            found = self._search(child, key)
            if found:
                return found
        return None

    def _search_subcategory(self, root, key):
        for child in root.children:
            if child.key == key:
                return child
        return None

    def pre_order_traversal(self, root, depth=0):
        if root:
            children_keys = [child.key for child in root.children]
            products = [product.product_name for product in root.products]
            print("  " * depth, root.key)
            if products:
                for product in products:
                    print("  " * (depth + 1), product)
            for child in root.children:
                self.pre_order_traversal(child, depth + 1)

    def print_products(self, root):
        if root:
            products = {}

            for child in root.children:
                sub_products = [product for product in child.products]
                products[child.key] = sub_products

            return products

    def print_subcategories(self, root):
        if root:
            children_keys = [child.key for child in root.children]

            print(children_keys)


        
def loadElectronicData():
    elec_tree = CategoryTree("electronics")
    temp = "electronics.csv"
    with open(
        temp,
        newline="",
    ) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            product = Electronics(
                row["Subcategory"],
                row["Product_Id"],
                row["Product_Name"],
                row["Product_Price"],
                row["Product_Quantity"],
            )
            elec_tree.add_subcategory("electronics", row["Subcategory"])
            elec_tree.add_electronics_to_subcategory(
                "electronics", row["Subcategory"], product
            )

    # elec_tree.print_subcategories(elec_tree.root)
    # elec_tree.pre_order_traversal(elec_tree.root)
    a = (elec_tree.print_products(elec_tree.root))
    # print(a['Mobiles'][0].sub_category)
    # print(a['Mobiles'][0].giveCategory())

    return a
# loadElectronicData()


def loadGroceryData():
    groc_tree = CategoryTree("grocery")
    temp = "grocery.csv"
    with open(
        temp,
        newline="",
    ) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            product = Grocery(
                row["Subcategory"],
                row["Product_Id"],
                row["Product_Name"],
                row["Product_Price"],
                row["Product_Quantity"],
            )
            groc_tree.add_subcategory("grocery", row["Subcategory"])
            groc_tree.add_grocery_to_subcategory(
                "grocery", row["Subcategory"], product
            )

    # elec_tree.print_subcategories(elec_tree.root)
    # elec_tree.pre_order_traversal(elec_tree.root)
    a = (groc_tree.print_products(groc_tree.root))
    # print(a['Mobiles'][0].sub_category)
    # print(a['Mobiles'][0].giveCategory())
    return a
# loadGroceryData()

def loadClothesData():
    cloth_tree = CategoryTree("clothes")
    temp = "clothes.csv"
    with open(
        temp,
        newline="",
    ) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            product = Clothes(
                row["Subcategory"],
                row["Product_Id"],
                row["Product_Name"],
                row["Product_Price"],
                row["Product_Size"],
                row["Product_Color"],
                row["Product_Quantity"],

            )
            cloth_tree.add_subcategory("clothes", row["Subcategory"])
            cloth_tree.add_clothes_to_subcategory(
                "clothes", row["Subcategory"], product
            )

    # elec_tree.print_subcategories(elec_tree.root)
    # cloth_tree.pre_order_traversal(cloth_tree.root)
    a = (cloth_tree.print_products(cloth_tree.root))
    # print(a['Shirts'][0].sub_category)
    # print(a['Shirts'][0].giveCategory())
    # print(a['Shirts'][0].color)
    # print(a)
    return a
# loadClothesData()

def loadMedicineData():
    med_tree = CategoryTree("medicine")
    temp = "medicine.csv"
    with open(
        temp,
        newline="",
    ) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            product = Medicine(
                row["Subcategory"],
                row["Product_Id"],
                row["Product_Name"],
                row["Product_Price"],
                row["Product_Quantity"],
            )
            med_tree.add_subcategory("medicine", row["Subcategory"])
            med_tree.add_medicines_to_subcategory(
                "medicine", row["Subcategory"], product
            )

    # elec_tree.print_subcategories(elec_tree.root)
    # med_tree.pre_order_traversal(med_tree.root)
    a = (med_tree.print_products(med_tree.root))
    # print(a['Mobiles'][0].sub_category)
    # print(a['Mobiles'][0].giveCategory())

    return a
# loadMedicineData()



def readFromCustomerData(Data):
    res = {}
    temp = 'CustomerData.csv'
    with open(temp, 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        
        # Skip the header if it exists
        header = next(csv_reader, None)

        for row in csv_reader:
            if row[-1] == datetime.today().strftime("%B"):
                res[(row[0], row[1], row[3], row[4])] = res.get((row[0], row[1], row[3], row[4]), 0) + int(row[5])
    heap = []
    
    for val, key in res.items():
        temp = Data
        if val[2] == "Electronics":
            temp = temp[0]
        elif val[2] == "Clothes":
            temp = temp[1]
        elif val[2] == "Medicine":
            temp = temp[2]
        elif val[2] == "Grocery":
            temp = temp[3]
        else:
            pass
        
        if val[3] not in temp:
            pass

        for obj in temp[val[3]]:
            if obj.product_name == val[1] and obj.product_id == val[0]:
                heap.append([-1 * key, obj])

    return(heap)
