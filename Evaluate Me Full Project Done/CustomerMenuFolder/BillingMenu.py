from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox
import sys
import typing
import sys,res
import sys,close
import sys,back
import sys,Image2
import sys,Img
import sys,pic
from datetime import datetime

import csv, heapq

from Helper import helper

class Ui_Form(object):
    def __init__(self, FormStack, cart) -> None:
        self.curForm = FormStack[-1]
        self.FormStack = FormStack
        self.cart = cart

    def setupUi(self):
        Form = self.curForm
        Form.setObjectName("Form")
        Form.resize(800, 591)
        self.widget = QtWidgets.QWidget(Form)
        self.widget.setGeometry(QtCore.QRect(0, 0, 801, 591))
        self.widget.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:1 rgba(255, 85, 127, 255));")
        self.widget.setObjectName("widget")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setGeometry(QtCore.QRect(70, 120, 651, 431))
        self.label.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label.setStyleSheet("background-image: url(:/img 3/IMG 2.jpg);")
        self.label.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label.setFrameShadow(QtWidgets.QFrame.Plain)
        self.label.setText("")
        self.label.setTextFormat(QtCore.Qt.AutoText)
        self.label.setPixmap(QtGui.QPixmap(":/img 3/IMG 2.jpg"))
        self.label.setScaledContents(True)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.label_8 = QtWidgets.QLabel(self.widget)
        self.label_8.setGeometry(QtCore.QRect(280, 0, 271, 51))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label_8.setFont(font)
        self.label_8.setStyleSheet("color:rgba(255,255,255,210);")
        self.label_8.setAlignment(QtCore.Qt.AlignCenter)
        self.label_8.setObjectName("label_8")
        self.label_9 = QtWidgets.QLabel(self.widget)
        self.label_9.setGeometry(QtCore.QRect(270, 40, 291, 41))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label_9.setFont(font)
        self.label_9.setStyleSheet("color:rgba(255,255,255,210);")
        self.label_9.setAlignment(QtCore.Qt.AlignCenter)
        self.label_9.setObjectName("label_9")
        self.label_2 = QtWidgets.QLabel(self.widget)
        self.label_2.setGeometry(QtCore.QRect(0, 0, 61, 51))
        self.label_2.setStyleSheet("background-image: url(back.png);")
        self.label_2.setText("")
        self.label_2.setPixmap(QtGui.QPixmap("back.png"))
        self.label_2.setScaledContents(True)
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.widget)
        self.label_3.setGeometry(QtCore.QRect(750, -10, 61, 61))
        self.label_3.setStyleSheet("background-image: url(cross.png);")
        self.label_3.setText("")
        self.label_3.setPixmap(QtGui.QPixmap("cross.png"))
        self.label_3.setScaledContents(True)
        self.label_3.setObjectName("label_3")
        self.label_2.mousePressEvent = self.go_back
        self.label_3.mousePressEvent = self.close_application
        self.label_10 = QtWidgets.QLabel(self.widget)
        self.label_10.setGeometry(QtCore.QRect(270, 70, 291, 41))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label_10.setFont(font)
        self.label_10.setStyleSheet("color:rgba(255,255,255,210);")
        self.label_10.setAlignment(QtCore.Qt.AlignCenter)
        self.label_10.setObjectName("label_10")
        self.label_4 = QtWidgets.QLabel(self.widget)
        self.label_4.setGeometry(QtCore.QRect(100, 150, 241, 41))
        self.label_4.setStyleSheet("background-color: rgb(0, 255, 127);\n"
"font: 87 12pt \"Arial Black\";")
        self.label_4.setAlignment(QtCore.Qt.AlignCenter)
        self.label_4.setObjectName("label_4")
        self.lineEdit = QtWidgets.QLineEdit(self.widget)
        self.lineEdit.setGeometry(QtCore.QRect(390, 150, 291, 41))
        self.lineEdit.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit.setObjectName("lineEdit")
        self.label_5 = QtWidgets.QLabel(self.widget)
        self.label_5.setGeometry(QtCore.QRect(100, 240, 241, 41))
        self.label_5.setStyleSheet("background-color: rgb(255, 85, 0);\n"
"font: 87 12pt \"Arial Black\";")
        self.label_5.setAlignment(QtCore.Qt.AlignCenter)
        self.label_5.setObjectName("label_5")
        self.lineEdit_2 = QtWidgets.QLineEdit(self.widget)
        self.lineEdit_2.setGeometry(QtCore.QRect(390, 240, 291, 41))
        self.lineEdit_2.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.label_6 = QtWidgets.QLabel(self.widget)
        self.label_6.setGeometry(QtCore.QRect(100, 340, 231, 41))
        self.label_6.setStyleSheet("background-color: rgb(0, 170, 255);\n"
"font: 87 12pt \"Arial Black\";")
        self.label_6.setAlignment(QtCore.Qt.AlignCenter)
        self.label_6.setObjectName("label_6")
        self.lineEdit_3 = QtWidgets.QLineEdit(self.widget)
        self.lineEdit_3.setGeometry(QtCore.QRect(390, 340, 291, 41))
        self.lineEdit_3.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.label_7 = QtWidgets.QLabel(self.widget)
        self.label_7.setGeometry(QtCore.QRect(260, 460, 231, 41))
        self.label_7.setStyleSheet("background-color: rgb(255, 255, 0);\n"
"font: 87 12pt \"Arial Black\";")
        self.label_7.setAlignment(QtCore.Qt.AlignCenter)
        self.label_7.setObjectName("label_7")
        self.label_7.mousePressEvent=self.Confirm_Order

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.label_8.setText(_translate("Form", "IJK Super Mart"))
        self.label_9.setText(_translate("Form", "Welcome To Customer Menu"))
        self.label_10.setText(_translate("Form", "Billing Section"))
        self.label_4.setText(_translate("Form", "Enter Address"))
        self.lineEdit.setPlaceholderText(_translate("Form", "Address"))
        self.label_5.setText(_translate("Form", "Enter Credit-Card Number"))
        self.lineEdit_2.setPlaceholderText(_translate("Form", "Card-Number"))
        self.label_6.setText(_translate("Form", "Total Amount"))
        self.lineEdit_3.setPlaceholderText(_translate("Form", "Amount"))
        self.label_7.setText(_translate("Form", "Order"))

        self.lineEdit_3.setReadOnly(True)
        self.lineEdit_3.setText(str(self.findTotalPrice()))

        self.label_7.mousePressEvent = self.onCLickOrder

    def findTotalPrice(self):
        Data = self.cart
        price = 0
        cur = Data.head.next
        while cur != Data.tail:
                obj = cur.val[0]
                qty = cur.val[1]

                price += qty * float(obj.product_price)

                cur = cur.next
        return price

    def onCLickOrder(self, event):
        if self.lineEdit.text() == "":
            reply=QMessageBox.warning(self.curForm, 'Sorry!', 'Enter Valid Adress',
                                QMessageBox.Ok )
            return
        if len(self.lineEdit_2.text()) < 5 or self.lineEdit_2.text().isnumeric() == False:
            reply=QMessageBox.warning(self.curForm, 'Sorry!', 'Enter Valid Card Number',
                                QMessageBox.Ok )
            self.lineEdit_2.setText("")
            return
        reply=QMessageBox.warning(self.curForm, 'Sucessfully', 'Your order has been placed sucessfully! Thank You!!',
                                     QMessageBox.Ok )
        if reply == QMessageBox.Ok:
            self.saveCartToCsv()
            self.FormStack.pop()  ## self
            self.FormStack.pop().close()  ## cart
            self.FormStack.pop().close()  ## category store
            ## now on customer menu
            try:
                self.FormStack[-1].curUi.saveAllDataProducts()
            except:
                print("data not saved successfully")
            self.modifyHeap(self.FormStack[-1].curUi.heap, self.FormStack[-1].curUi.cart)
            self.FormStack[-1].curUi.cart.clear()
            self.FormStack[-1].curUi.tempFillBox()
            self.FormStack[-1].show()
            self.curForm.close()

    def modifyHeap(self, heap, data):
        # print(heap)
        # print(data)

        for obj, qty in data.items():
            temp = None
            for i in heap:
                if i[1] == obj:
                    temp = [i[0] - qty, obj]
                    heap.remove(i)
                    break
            if temp == None:
                temp = [-1 * qty, obj]
            heap.append(temp)
        self.FormStack[-1].curUi.removeDuplicatesFromHeap(heap)
        heapq.heapify(heap)
        # print(heap)

            


    def saveCartToCsv(self):
        fieldnames = ['ProductId', 'ProductName', 'ProductPrice', 'ProductCategory', 'ProductSubCategory', 'TotalSold', 'Month']
        temp = "CustomerData.csv"
        with open(temp, 'a', newline='') as csvfile:
            writer = csv.writer(csvfile)

            # Write header
            # writer.writerow(['ProductId', 'ProductName', 'ProductPrice', 'ProductCategory', 'ProductSubCategory', 'TotalSold','Month'])

            # Write data
                
            Data = self.cart
            cur = Data.head.next
            while cur != Data.tail:
                    obj = cur.val[0]
                    qty = cur.val[1]

                    writer.writerow([obj.product_id, obj.product_name, obj.product_price, obj.giveCategory(), obj.sub_category, qty,  datetime.today().strftime("%B")])
                    
                    cur = cur.next




    def go_back(self, event):
        self.FormStack.pop()
        self.FormStack[-1].show()
        self.curForm.close()

    def close_application(self, event):
        reply = QMessageBox.question(self.curForm, 'Exit', 'Are you sure you want to exit?',
                                     QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.FormStack[0].quit()
    
    def Confirm_Order(self,event):
        print('Order Button Clicked')
        reply=QMessageBox.warning(Form, 'Sucessfully', 'Your order has been placed sucessfully! Thank You!!',
                                     QMessageBox.Ok )


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())
