from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox
import sys
import typing
import sys,res
import sys,close
import sys,back
import sys,Image2
import sys,Img
import sys,pic

from PyQt5.QtWidgets import QApplication, QMainWindow, QTableWidget, QTableWidgetItem, QPushButton, QHBoxLayout, QWidget, QSpinBox

from Helper import DLL
import BillingMenu

class Ui_Form(object):
    def __init__(self, FormStack, cart) -> None:
        self.curForm = FormStack[-1]
        self.FormStack = FormStack

        dll = DLL.LinkedList()
        for key, val in cart.items():
            dll.insertEnd([key, val])
        self.cart = dll

    def setupUi(self):
        Form = self.curForm
        Form.setObjectName("Form")
        Form.resize(951, 590)
        self.widget = QtWidgets.QWidget(Form)
        self.widget.setGeometry(QtCore.QRect(0, 0, 951, 591))
        self.widget.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:1 rgba(255, 85, 127, 255));")
        self.widget.setObjectName("widget")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setGeometry(QtCore.QRect(70, 120, 841, 431))
        self.label.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label.setStyleSheet("background-image: url(:/img 3/IMG 2.jpg);")
        self.label.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label.setFrameShadow(QtWidgets.QFrame.Plain)
        self.label.setText("")
        self.label.setTextFormat(QtCore.Qt.AutoText)
        self.label.setPixmap(QtGui.QPixmap(":/img 3/IMG 2.jpg"))
        self.label.setScaledContents(True)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.label_8 = QtWidgets.QLabel(self.widget)
        self.label_8.setGeometry(QtCore.QRect(340, 0, 271, 51))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label_8.setFont(font)
        self.label_8.setStyleSheet("color:rgba(255,255,255,210);")
        self.label_8.setAlignment(QtCore.Qt.AlignCenter)
        self.label_8.setObjectName("label_8")
        self.label_9 = QtWidgets.QLabel(self.widget)
        self.label_9.setGeometry(QtCore.QRect(330, 40, 291, 41))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label_9.setFont(font)
        self.label_9.setStyleSheet("color:rgba(255,255,255,210);")
        self.label_9.setAlignment(QtCore.Qt.AlignCenter)
        self.label_9.setObjectName("label_9")
        self.label_2 = QtWidgets.QLabel(self.widget)
        self.label_2.setGeometry(QtCore.QRect(0, 0, 61, 51))
        self.label_2.setStyleSheet("background-image: url(back.png);")
        self.label_2.setText("")
        self.label_2.setPixmap(QtGui.QPixmap("back.png"))
        self.label_2.setScaledContents(True)
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.widget)
        self.label_3.setGeometry(QtCore.QRect(900, -10, 61, 61))
        self.label_3.setStyleSheet("background-image: url(cross.png);")
        self.label_3.setText("")
        self.label_3.setPixmap(QtGui.QPixmap("cross.png"))
        self.label_3.setScaledContents(True)
        self.label_3.setObjectName("label_3")
        self.label_2.mousePressEvent = self.go_back
        self.label_3.mousePressEvent = self.close_application
        self.label_10 = QtWidgets.QLabel(self.widget)
        self.label_10.setGeometry(QtCore.QRect(330, 70, 291, 41))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label_10.setFont(font)
        self.label_10.setStyleSheet("color:rgba(255,255,255,210);")
        self.label_10.setAlignment(QtCore.Qt.AlignCenter)
        self.label_10.setObjectName("label_10")
        self.tableWidget = QtWidgets.QTableWidget(self.widget)
        self.tableWidget.setGeometry(QtCore.QRect(130, 150, 741, 341))
        self.tableWidget.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.pushButton = QtWidgets.QPushButton(self.widget)
        self.pushButton.setGeometry(QtCore.QRect(700, 502, 161, 41))
        self.pushButton.setStyleSheet("background-color: rgb(170, 85, 255);\n"
"font: 87 12pt \"Arial Black\";")
        self.pushButton.setObjectName("pushButton")
        self.pushButton.mousePressEvent=self.next
        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.label_8.setText(_translate("Form", "IJK Super Mart"))
        self.label_9.setText(_translate("Form", "Welcome To Customer Menu"))
        self.label_10.setText(_translate("Form", "Overview Purchased Items"))
        self.pushButton.setText(_translate("Form", "Confirm and Next"))

        self.setUpTable()  ############# addition

    
    def go_back(self, event):
        self.FormStack.pop()
        self.FormStack[-1].show()
        try:
            self.FormStack[-1].curUi.setUpTable()
        except:
            pass
        self.curForm.close()

    def close_application(self, event):
        reply = QMessageBox.question(self.curForm, 'Exit', 'Are you sure you want to exit?',
                                     QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.FormStack[0].quit()
    
    def next(self,event):
        if self.cart.giveLen() == 0:
            reply=QMessageBox.warning(self.curForm, 'Empty', 'Cart Empty',
                                QMessageBox.Ok )
            return
        reply=QMessageBox.question(self.curForm, 'Billing Alert', 'Are you sure you want to move Next?',
                                     QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            nextForm = QtWidgets.QWidget()
            self.FormStack.append(nextForm)
            newUi = BillingMenu.Ui_Form(self.FormStack, self.cart)
            newUi.setupUi()
            nextForm.show()

            self.curForm.hide()
        
        
    def setUpTable(self):
        Data = self.cart
        table = self.tableWidget

        table.clearContents()
        table.setRowCount(Data.giveLen())

        table.setColumnCount(6)
        table.setHorizontalHeaderLabels(['Name', 'Price', 'Quantity', 'Total Price', 'Items to Remove', 'Remove from Cart'])
        table.verticalHeader().hide()

        cur = Data.head.next
        index = 0
        while cur != Data.tail:
                obj = cur.val[0]
                qty = cur.val[1]

                whl = QSpinBox()
                table.setCellWidget(index, 4, whl)
                whl.setMaximum(int(qty))
                whl.setMinimum(min(1, int(qty)))

                btn = QPushButton("remove from cart")

                btn.clicked.connect(lambda checked, cur = cur, obj = obj, spinWheel = whl: self.removeFromCartButtonClick(cur, obj, spinWheel))
                table.setCellWidget(index, 5, btn)
                table.setItem(index, 0, QTableWidgetItem(obj.product_name))
                table.setItem(index, 1, QTableWidgetItem(obj.product_price))
                table.setItem(index, 2, QTableWidgetItem(str(qty)))
                table.setItem(index, 3, QTableWidgetItem(str(qty * float(obj.product_price))))
                index += 1
                cur = cur.next

    def removeFromCartButtonClick(self, cur, obj, spinWheel):
        
        obj.quantity = str(int(obj.quantity) + int(spinWheel.value()))
        cur.val[1] -= int(spinWheel.value())
        if cur.val[1] == 0:
            self.cart.remove(cur)
        
        self.setUpTable()
        return

if __name__ == "__main__":
    print("here")
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())